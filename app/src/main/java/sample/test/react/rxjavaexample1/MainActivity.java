package sample.test.react.rxjavaexample1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import hu.akarnokd.rxjava.interop.RxJavaInterop;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity {

    EditText etEmail;
    TextView textEmailAlert;
    EditText etPassword;
    TextView textPasswordAlert;
    EditText etPasswordConfirmation;
    TextView textPasswordConfirmationAlert;
    Button btnSubmit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Inisialisasi form
        etEmail = (EditText) findViewById(R.id.et_email);
        textEmailAlert = (TextView) findViewById(R.id.text_email_alert);
        etPassword = (EditText) findViewById(R.id.et_password);
        textPasswordAlert = (TextView) findViewById(R.id.text_password_alert);
        etPasswordConfirmation = (EditText) findViewById(R.id.et_password_confirmation);
        textPasswordConfirmationAlert = (TextView) findViewById(R.id.text_password_confirmation_alert);
        btnSubmit = (Button) findViewById(R.id.btn_submit);

        // Inisialisasi state (keadaan) default form
        textEmailAlert.setVisibility(View.GONE);
        textPasswordAlert.setVisibility(View.GONE);
        textPasswordConfirmationAlert.setVisibility(View.GONE);
        btnSubmit.setEnabled(false);




        Observable<Boolean> passwordStream = RxTextView.textChanges(etPassword)
                .map(new Func1<CharSequence, Boolean>() {
                    @Override
                    public Boolean call(CharSequence charSequence) {
                        return !TextUtils.isEmpty(charSequence) && charSequence.length() >3;
                    }
                });

        // Return true jika email user sudah terpakai
        Observable<Boolean> emailStream = RxTextView.textChanges(etEmail)
                .map(new Func1<CharSequence, String>() {
                    @Override
                    public String call(CharSequence charSequence) {
                        return charSequence.toString();
                    }
                })
                .filter(new Func1<String, Boolean>() {
                    @Override
                    public Boolean call(String input) {
                        return input.length() > 3;
                    }
                })
                .debounce(100, TimeUnit.MILLISECONDS)
                .flatMap(new Func1<String, Observable<Boolean>>() {
                    @Override
                    public Observable<Boolean> call(String input) {
                        return checkIfEmailExistFromAPI(input);
                    }
                });

        emailStream.subscribe(emailObserver);
        //passwordStream.subscribe(passObserver);


    }

    public Observable<Boolean> checkIfEmailExistFromAPI(final String input){
        List<String> list = Arrays.asList("Hello", "Streams", "Not");
        return Observable.from(list).contains(input);
        /*
        return Observable.from(list).flatMap(new Func1<String, Observable<String>>() {
            @Override
            public Observable<String> call(String s) {
                return Observable.just(s);
            }
        }).contains(input).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread());*/
    }

    public void showEmailExistAlert(boolean value){
        if(value) {
            textEmailAlert.setText(getString(R.string.email_exist_alert));
            textEmailAlert.setVisibility(View.VISIBLE);
        } else {
            textEmailAlert.setVisibility(View.GONE);
        }
    }
    Observer<Boolean> emailObserver = new Observer<Boolean>() {
        @Override
        public void onCompleted() {
            Log.d("rx","Email stream completed");
        }

        @Override
        public void onError(Throwable e) {
            Log.d("rx",e.getMessage());
        }

        @Override
        public void onNext(Boolean emailExist) {
            Log.d("emailObserver",String.valueOf(emailExist.booleanValue()));
            showEmailExistAlert(emailExist.booleanValue());
        }
    };
/*
    Observer<Boolean> passObserver = new Observer<Boolean>() {
        @Override
        public void onCompleted() {
            Log.d("rx","Email stream completed");
        }

        @Override
        public void onError(Throwable e) {
            Log.d("rx",e.getMessage());
        }

        @Override
        public void onNext(Boolean emailExist) {
            Log.d("emailObserver",String.valueOf(emailExist.booleanValue()));
            showPasswordMinimalAlert(emailExist.booleanValue());
        }
    };



    public void showPasswordMinimalAlert(boolean value){
        if(value) {
            textPasswordAlert.setText(getString(R.string.password_minimal_alert));
            textPasswordAlert.setVisibility(View.VISIBLE);
        } else {
            textPasswordAlert.setVisibility(View.GONE);
        }
    }



    public Observable <List<String>> getUserList()
    {
        List<String> list = Arrays.asList("Hello", "Streams", "Not");
        //Observable.from(list)
        return list;
    }*/
}
