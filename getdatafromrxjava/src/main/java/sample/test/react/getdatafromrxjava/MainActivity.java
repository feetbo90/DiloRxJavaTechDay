package sample.test.react.getdatafromrxjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.jakewharton.rxbinding.widget.TextViewTextChangeEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import adapter.ContactAdapter;
import model.Model;
import modeldua.Contact;
import modeldua.Contacts;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import service.ModelClient;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private Subscription subscription;
    private ContactAdapter adapter = new ContactAdapter();
    List<Contact> move = new ArrayList<Contact>();
    EditText editTextUsername;
    Button buttonSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final ListView listView = (ListView) findViewById(R.id.list_view_repos);
        listView.setAdapter(adapter);

        editTextUsername = (EditText) findViewById(R.id.edit_text_username);

        //getJsonCoy();
        AmbilData();


    }

    public void AmbilData()
    {
        Observable<List<Contact>> emailStream = RxTextView.textChangeEvents(editTextUsername)
                .map(new Func1<TextViewTextChangeEvent, String>() {
                    @Override
                    public String call(TextViewTextChangeEvent charSequence) {
                        Log.d("nilai", "berubah");
                        return charSequence.text().toString();
                    }
                })
                .filter(new Func1<String, Boolean>() {
                    @Override
                    public Boolean call(String input) {
                        Log.d("nilai", "berubah2");
                        return input.length() > 3;
                    }
                })
                .debounce(100,TimeUnit.MILLISECONDS)
                .flatMap(new Func1<String, Observable<List<Contact>> >() {
                    @Override
                    public Observable<List<Contact>>  call(String input) {
                        return getData();
                    }
                });

        emailStream.subscribe(new Observer<List<Contact>>() {
                    @Override
                    public void onCompleted() {
                        Log.d("nilai", "completed");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("nilai", e.getMessage());
                    }

                    @Override
                    public void onNext(List<Contact> contacts) {
                        Log.d("nilai", "masuk");
                        move.clear();
                        for(Contact a : contacts)
                        {
                            if(a.getName().contains(editTextUsername.getText().toString()))
                            {
                                move.add(a);
                            }
                        }
                        adapter.setContactAdapters(move);
                    }
                });
    }

    @Override protected void onDestroy() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
        super.onDestroy();
    }

    public Observable<List<Contact>> getData(){
        return ModelClient.getInstance().getJsonCoy()
                .map(new Func1<Contacts, List<Contact>>() {
                    @Override
                    public List<Contact> call(Contacts cont) {
                        // rx.Observable.from(cont);
                        List<Contact> contact = cont.getContacts();

                        return contact;
                    }
                }).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private void getJsonCoy() {
        int i = 0;
        subscription = RxTextView.textChanges(editTextUsername).map(new Func1<CharSequence, String>() {
            @Override
            public String call(CharSequence charSequence) {
                Log.d("nilai", "berubah");
                return charSequence.toString();
            }
                })
                .filter(new Func1<String, Boolean>() {
                    @Override
                    public Boolean call(String input) {
                        return input.length() > 3;
                    }
                })
                .debounce(100,TimeUnit.MILLISECONDS)
                .flatMap(new Func1<String, Observable<List<Contact>>> () {
                    @Override
                    public Observable<List<Contact>> call(String input) {
                        return getData();
                    }
                })
                .subscribe(new Observer<List<Contact>>() {
                    @Override
                    public void onCompleted() {
                        Log.d("nilai", "completed");
                    }

                    @Override
                    public void onError(Throwable e) {
                        //if()
                        Log.d("nilai", e.getMessage());
                    }

                    @Override
                    public void onNext(List<Contact> contacts) {
                        Log.d("nilai", "masuk");
                        move.clear();
                        for(Contact a : contacts)
                        {
                            if(a.getName().contains(editTextUsername.getText().toString()))
                            {
                                move.add(a);
                                Log.d("ada", a.getName());
                            }
                        }
                        adapter.setContactAdapters(move);
                    }
                });

    }
}
