package sample.test.react.getdatafromrxjava;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxTextView;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.functions.Func3;


/**
 * Created by root on 09/03/17.
 */

public class ExampleActivity extends AppCompatActivity {

    EditText etPassword;
    TextView textPasswordAlert;
    EditText etPasswordConfirmation;
    TextView textPasswordConfirmationAlert;
    Button btnSubmit;
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reactive_demo);

        etPassword = (EditText) findViewById(R.id.et_password);
        textPasswordAlert = (TextView) findViewById(R.id.text_password_alert);
        etPasswordConfirmation = (EditText) findViewById(R.id.et_password_confirmation);
        textPasswordConfirmationAlert = (TextView) findViewById(R.id.text_password_confirmation_alert);
        btnSubmit = (Button) findViewById(R.id.btn_submit);

        // Inisialisasi state (keadaan) default form
        textPasswordAlert.setVisibility(View.GONE);
        textPasswordConfirmationAlert.setVisibility(View.GONE);
        btnSubmit.setEnabled(false);

        //Confirmation();

        // Sample Pembuatan Observable dan Transformasi
            //basicRx();
            //basicjustRx();
     //       basicFromRx();
          //  Filter();
           // Map();
            FlatMap();
    }

    public void Confirmation()
    {
        Observable<Boolean> passwordStream = RxTextView.textChanges(etPassword)
                .map(new Func1<CharSequence, Boolean>() {
                    @Override
                    public Boolean call(CharSequence charSequence) {
                        return !TextUtils.isEmpty(charSequence)
                                && charSequence.toString().trim().length() > 4;
                    }
                });

        Observable<Boolean> passwordConfirmationStream = Observable.merge(
                RxTextView.textChanges(etPassword)
                        .map(new Func1<CharSequence, Boolean>() {
                            @Override
                            public Boolean call(CharSequence charSequence) {
                                boolean Test = false;
                                if(charSequence.toString().trim().equals(etPasswordConfirmation.getText().toString()))
                                {
                                    Test = true;
                                }
                                return Test;
                            }
                        }),
                RxTextView.textChanges(etPasswordConfirmation)
                        .map(new Func1<CharSequence, Boolean>() {
                            @Override
                            public Boolean call(CharSequence charSequence) {
                                return charSequence.toString().trim().equals(etPassword.getText().toString());
                            }
                        })
        );


        Observable<Boolean> emptyFieldStream = Observable.combineLatest(
                RxTextView.textChanges(etPassword)
                        .map(new Func1<CharSequence, Boolean>() {
                            @Override
                            public Boolean call(CharSequence charSequence) {
                                return !TextUtils.isEmpty(charSequence);
                            }
                        }),
                RxTextView.textChanges(etPasswordConfirmation)
                        .map(new Func1<CharSequence, Boolean>() {
                            @Override
                            public Boolean call(CharSequence charSequence) {
                                return !TextUtils.isEmpty(charSequence);
                            }
                        }),
                new Func2<Boolean, Boolean, Boolean>() {
                    @Override
                    public Boolean call(Boolean passwordEmpty, Boolean passwordConfirmationEmpty) {
                        return passwordEmpty || passwordConfirmationEmpty;
                    }
                }


        );

        Observable<Boolean> invalidFieldsStream = Observable.combineLatest(

                passwordStream,
                passwordConfirmationStream,
                emptyFieldStream, new Func3<Boolean, Boolean, Boolean, Boolean>() {
                    @Override
                    public Boolean call( Boolean passwordInvalid, Boolean passwordConfirmationInvalid, Boolean emptyFieldExist) {
                        return  passwordInvalid && passwordConfirmationInvalid && emptyFieldExist;
                    }
                });

        Observer<Boolean> passwordObserver = new Observer<Boolean>() {
            @Override
            public void onCompleted() {
                Log.d("rx","Password stream completed");
            }

            @Override
            public void onError(Throwable e) {
                Log.d("rx",e.getMessage());
            }

            @Override
            public void onNext(Boolean passwordLessThanLimit) {
                Log.d("passwordObserver",String.valueOf(passwordLessThanLimit.booleanValue()));
                //Toast.makeText(ExampleActivity.this, "Password Sukses", Toast.LENGTH_LONG).show();
                //showPasswordMinimalAlert(passwordLessThanLimit.booleanValue());
            }
        };

        Observer<Boolean> passwordConfirmationObserver = new Observer<Boolean>() {
            @Override
            public void onCompleted() {
                Log.d("rx","Password confirmation stream completed");
            }

            @Override
            public void onError(Throwable e) {
                Log.d("rx",e.getMessage());
            }

            @Override
            public void onNext(Boolean passwordConfirmationDontMatch) {
                Log.d("passwordConfirmation",String.valueOf(passwordConfirmationDontMatch.booleanValue()));
                //Toast.makeText(ExampleActivity.this, "Confirmation Password Match", Toast.LENGTH_LONG).show();
                //showPasswordConfirmationAlert(passwordConfirmationDontMatch.booleanValue());
            }
        };


        Observer<Boolean> invalidFieldsObserver = new Observer<Boolean>() {
            @Override
            public void onCompleted() {
                Log.d("rx","All field valid stream completed");
            }

            @Override
            public void onError(Throwable e) {
                Log.d("rx",e.getMessage());
            }

            @Override
            public void onNext(Boolean invalidFieldExist) {
                Log.d("invalidFieldsStream",String.valueOf(invalidFieldExist.booleanValue()));
                btnSubmit.setEnabled(invalidFieldExist);
            }
        };
        passwordStream.subscribe(passwordObserver);

        passwordConfirmationStream.subscribe(passwordConfirmationObserver);

        invalidFieldsStream.subscribe(invalidFieldsObserver);
    }

/*
    > 90 A
    70 90 B+
    50 69 B
    C+
*/

    public void RataNilai() {
        Integer[] items = {80, 100, 70, 80, 40, 50};
        Observable myObs = Observable.from(items).flatMap(new Func1<Integer, Observable<String>>() {
            @Override
            public Observable<String> call(Integer integer) {
                String nilai = null;
                if (integer > 90) {
                    nilai = "A";
                } else if (integer > 70 && integer <= 90) {
                    nilai = "B+";
                }
                return Observable.just(nilai);
            }
        });
    myObs.subscribe(new Subscriber<String>() {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onNext(String o) {
            Log.d("Nilai" , o);
        }
    });
    }

    public void FlatMap(){
        Integer[] items = { 80, 100, 70, 80, 40, 50 };
        Observable myObservable = Observable.from(items).map(new Func1<Integer, Integer>() {
            @Override
            public Integer call(Integer integer) {
                integer = integer * 3;
                return integer;
            }

        }).filter(new Func1<Integer, Boolean>() {
            @Override
            public Boolean call(Integer integer) {
                return (integer % 2 == 1);
            }
        }).flatMap(new Func1<Integer, Observable<String>>() {
                       @Override
                       public Observable<String> call(Integer integer) {
                           String kalimat = "ini Bilangan Ganjil :" + integer;
                           return Observable.just(kalimat);
                       }
                   });

                myObservable.subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                        Toast.makeText(ExampleActivity.this, "Selesai", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(String o) {
                        Log.d("FlatMap", o);
                        Toast.makeText(ExampleActivity.this, "dari Map " + o, Toast.LENGTH_LONG).show();
                    }
                });
    }

    public void Map(){
        Integer[] items = { 0, 1, 2, 3, 4, 5 };
        Observable myObservable = Observable.from(items).map(new Func1<Integer, Integer>() {
            @Override
            public Integer call(Integer integer) {
                integer = integer * 2;
                return integer;
            }

        }).filter(new Func1<Integer, Boolean>() {
            @Override
            public Boolean call(Integer integer) {
                return (integer % 2 == 0);
            }
        });

        myObservable.subscribe(new Subscriber<Integer>() {
            @Override
            public void onCompleted() {
                Toast.makeText(ExampleActivity.this, "Selesai", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Integer o) {
                Toast.makeText(ExampleActivity.this, "dari Map "+ o, Toast.LENGTH_LONG).show();
            }
        });

    }

    public void Filter(){
        Observable.just(1, 2, 3, 4, 5)
                .filter(new Func1<Integer, Boolean>() {
                    @Override
                    public Boolean call(Integer item) {
                        return( item < 4 );
                    }
                }).subscribe(new Subscriber<Integer>() {
            @Override
            public void onNext(Integer item) {
                Log.d("Filter", ""+item);
                Toast.makeText(ExampleActivity.this, "dari Filter "+ item, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(Throwable error) {
                System.err.println("Error: " + error.getMessage());
            }

            @Override
            public void onCompleted() {
                Log.d("Filter", "salah");
                Toast.makeText(ExampleActivity.this, "Selesai",  Toast.LENGTH_LONG).show();
            }
        });
    }

    public void basicFromRx(){
        String[] items = { "Hallo", "Saya", "Rx Java" };

        Observable myObservable = Observable.from(items);

        myObservable.subscribe(new Subscriber<String>() {
            @Override
            public void onCompleted() {
                Toast.makeText(ExampleActivity.this, "Selesai", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(String o) {
                Toast.makeText(ExampleActivity.this, "dari From "+ o, Toast.LENGTH_LONG).show();
            }
        });

    }


    public void basicjustRx(){
        Observable.just("1").subscribe(new Action1<String>() {
            @Override
            public void call(String s) {
                Toast.makeText(ExampleActivity.this, ""+ s, Toast.LENGTH_LONG).show();
            }
        });
    }


    public void basicRx(){


        Observable<Integer> myObservable = Observable.create(
                new Observable.OnSubscribe<Integer>() {
                    @Override
                    public void call(Subscriber<? super Integer> sub) {
                        for (int i = 1; i<=5; i++) {
                            sub.onNext(i);
                        }
                        sub.onCompleted();
                    }
                }
        );


        Subscriber<Integer> mySubscriber = new Subscriber<Integer>() {
            @Override
            public void onNext(Integer s) { Log.d("masuk", "masuk data ke : "+ s);
            Toast.makeText(ExampleActivity.this, ""+ s, Toast.LENGTH_LONG).show();}

            @Override
            public void onCompleted() { Log.d("masuk", "Completed"); Toast.makeText(ExampleActivity.this, "Completed", Toast.LENGTH_LONG).show();}

            @Override
            public void onError(Throwable e) { }
        };
        myObservable.subscribe(mySubscriber);
    }
}
