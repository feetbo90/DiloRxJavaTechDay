
package modeldua;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Contacts {

    @SerializedName("contacts")
    @Expose
    private List<Contact> contacts = new ArrayList<Contact>();

    /**
     * 
     * @return
     *     The contacts
     */
    public List<Contact> getContacts() {
        return contacts;
    }

    /**
     * 
     * @param contacts
     *     The contacts
     */
    public void setContacts(List<Contact> contacts) {
        this.contacts = contacts;
    }

}
