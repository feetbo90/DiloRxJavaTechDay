package adapter;

/**
 * Created by root on 13/02/17.
 */
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import modeldua.Contact;
import sample.test.react.getdatafromrxjava.R;

/**
 * Created by chris on 6/1/16.
 */
public class ContactAdapter extends BaseAdapter {

    private List<Contact> contacts = new ArrayList<>();

    @Override public int getCount() {
        return contacts.size();
    }

    @Override public Contact getItem(int position) {
        if (position < 0 || position >= contacts.size()) {
            return null;
        } else {
            return contacts.get(position);
        }
    }

    @Override public long getItemId(int position) {
        return position;
    }

    @Override public View getView(int position, View convertView, ViewGroup parent) {
        final View view = (convertView != null ? convertView : createView(parent));
        final ContactViewHolder viewHolder = (ContactViewHolder) view.getTag();
        viewHolder.setGitHubRepo(getItem(position));
        return view;
    }

    public void setContactAdapters(@Nullable List<Contact> repos) {
        if (repos == null) {
            return;
        }
        contacts.clear();
        contacts.addAll(repos);
        notifyDataSetChanged();
    }

    private View createView(ViewGroup parent) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.item_layout, parent, false);
        final ContactViewHolder viewHolder = new ContactViewHolder(view);
        view.setTag(viewHolder);
        return view;
    }

    public void add(Contact cont) {
        contacts.add(cont);
        notifyDataSetChanged();
    }

    private static class ContactViewHolder {

        private TextView textRepoName;
        private TextView textRepoDescription;
        private TextView textLanguage;
        private TextView textStars;

        public ContactViewHolder(View view) {
            textRepoName = (TextView) view.findViewById(R.id.text_repo_name);
            textRepoDescription = (TextView) view.findViewById(R.id.text_repo_description);
            textLanguage = (TextView) view.findViewById(R.id.text_language);
            textStars = (TextView) view.findViewById(R.id.text_stars);
        }

        public void setGitHubRepo(Contact gitHubRepo) {
            textRepoName.setText(gitHubRepo.getName());
            textRepoDescription.setText(gitHubRepo.getAddress());
            textLanguage.setText("Email: " + gitHubRepo.getEmail());
            textStars.setText("Mobile: " + gitHubRepo.getPhone().getMobile());
        }
    }
}