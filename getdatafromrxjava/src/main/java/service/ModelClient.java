package service;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import model.Model;
import modeldua.Contacts;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

/**
 * Created by root on 12/02/17.
 */

public class ModelClient {
//
 //private static final String url =  "https://restcountries.eu/rest/v1/";
private static final String url = "http://api.androidhive.info/";
    private static ModelClient instance;
    private DroidService droidService;

    private ModelClient() {
        final Gson gson =
                new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create();
        final Retrofit retrofit = new Retrofit.Builder().baseUrl(url)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        droidService = retrofit.create(DroidService.class);
    }

    public static ModelClient getInstance() {
        if (instance == null) {
            instance = new ModelClient();
        }
        return instance;
    }

    public Observable<Contacts> getJsonCoy() {
        return droidService.getJson();
    }
}

