package service;

/**
 * Created by root on 12/02/17.
 */

import java.util.List;

import model.Model;
import modeldua.Contacts;
import retrofit2.http.GET;
import rx.Observable;

/** contacts/
 * Created by chris on 6/1/16.
 */
public interface DroidService {
    //@GET("all") Observable<List<Model>> getJson();
    @GET("contacts/") Observable<Contacts> getJson();
}